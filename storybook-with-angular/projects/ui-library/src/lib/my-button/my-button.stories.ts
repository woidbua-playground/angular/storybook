import type { Meta, StoryObj } from '@storybook/angular';
import { MyButtonComponent } from './my-button.component';

const meta: Meta<MyButtonComponent> = {
  component: MyButtonComponent,
  tags: ['autodocs'],
  args: {
    title: 'Button',
  },
};

export default meta;
type Story = StoryObj<MyButtonComponent>;

export const Primary: Story = {
  args: {
    title: 'Hello World!',
  },
};
