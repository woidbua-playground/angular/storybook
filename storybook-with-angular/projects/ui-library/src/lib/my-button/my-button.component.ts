import { Component, Input } from '@angular/core';
import { TranslatePipe } from '../translate.pipe';

@Component({
  selector: 'lib-my-button',
  standalone: true,
  imports: [TranslatePipe],
  templateUrl: './my-button.component.html',
  styleUrl: './my-button.component.css',
})
export class MyButtonComponent {
  @Input() title: string | undefined;
}
