import { Pipe, type PipeTransform } from '@angular/core';

interface Translations {
  [key: string]: TranslationItem;
}

interface TranslationItem {
  [key: string]: string;
}

@Pipe({ name: 'translate', standalone: true })
export class TranslatePipe implements PipeTransform {
  private translations: Translations = {
    'button.show-more': {
      de: 'Mehr anzeigen',
      en: 'Show more',
      es: 'Mostrar más',
    },
    'button.show-less': {
      de: 'Weniger anzeigen',
      en: 'Show less',
      es: 'Mostrar menos',
    },
  };

  transform(key: string): string {
    const lang = localStorage.getItem('lang') || 'de';

    const translationValues = this.translations[key];
    if (!translationValues) {
      return key;
    }

    return translationValues[lang] || key;
  }
}
