# Commands

## Project/Library Creation

1. `ng new <project-name> --create-application=false`
2. `ng g library ui-library`
3. `ng g c my-button --project=ui-library`

## Storybook Integration

1. `npx storybook@latest init`
2. `ng run ui-library:storybook`

## Deployment

1. `ng build --configuration=production ui-library` or `ng build ui-library --configuration=production --watch`
2. `cd dist/ui-library` and `npm publish`
